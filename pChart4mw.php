<?php
/**
 * pChart4mw.php
 * provide tags for drawing charts easily using pChart.
 * written by Robert Horlings
 * http://www.mediawiki.org/wiki/Extension:pChart4mw
 *
 * To configure the functionality of this extension include the following in your
 * LocalSettings.php file:
 *
 * $wgPChart4mwDefaults = Array ( "size" => "200x120" );
 * $wgPChart4mwLinesDefaults = Array ( "grid" => "xy", "ymin" => "0", "ylabel" => "4");
 * $wgPChart4mwBarsDefaults = Array ( "grid" => "y", "ymin" => "0", "ylabel" => "4" );
 * $wgPChart4mwPieDefaults = Array ( "3d" => "3d" );

 * require_once( "$IP/extensions/pChart4mw/pChart4mw.php" );
 */

if (function_exists( 'wfLoadExtension' )) {
	wfLoadExtension( 'pChart4mw' );
	// Keep i18n globals so mergeMessageFileList.php doesn't break
	$wgMessagesDirs['pChart4mw'] = __DIR__ . '/i18n';
	wfWarn(
		'Deprecated PHP entry point used for pChart4mw extension. Please use wfLoadExtension instead, ' .
		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return;
} else {
	die( 'This version of the pChart4mw extension requires MediaWiki 1.25+' );
}
