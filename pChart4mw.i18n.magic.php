<?php
/**
 * pChart4mw
 *
 * @author		Alexia E. Smith
 * @license		All Rights Reserved
 * @package		GPLv3
 *
**/

$magicWords = [];
$magicWords['en'] = [
	'pBars'		=> [0, 'pBars'],
	'pLines'	=> [0, 'pLines'],
	'pBubble'	=> [0, 'pBubble'],
	'pPie'		=> [0, 'pPie'],
	'pScatter'	=> [0, 'pScatter'],
	'pRadar'	=> [0, 'pRadar']
];
